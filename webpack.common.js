const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')

const entry = "./webapp/public";
const dist = "dist";

module.exports = {
  entry: {
    app: path.join(__dirname, entry+"/client.js")
  },
  module: {
    loaders: [
      {
        test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader',
        query: {
          presets: ['react', 'flow', ['es2015', { modules: false } ], 'stage-0'],
          plugins: ['react-html-attrs','transform-class-properties','transform-decorators-legacy', 'transform-runtime']
        }
      },
      {
        test: /\.css$/, exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [{ loader: 'css-loader', options: { modules: true, minimize: true } }]
        })
      },
      {
        test: /\.css$/, include: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [{ loader: 'css-loader', options: {minimize: true } }]
        })
      },
      {
        test: /\.(png|jpg)$/, exclude: /node_modules/, loader: 'file-loader',
        options: { publicPath: '/'}
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([dist]),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.[hash].js',
      minChunks (module) {
        return module.context && module.context.indexOf('node_modules') >= 0;
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "manifest",
      filename: 'manifest.[hash].js',
      minChunks: Infinity
    }),
    new ExtractTextPlugin({
      filename: '[name].[contenthash].css',
      allChunks: true
    }),
    new CopyWebpackPlugin([
      {from: path.join(__dirname, entry+"/static"), to: path.join(__dirname, dist+"/static")}
    ]),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, entry+"/index.html"),
      filename: 'index.html',
      minify: {
        collapseWhitespace: true,
        collapseInlineTagWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true
      }
    })
  ],
  output: {
    path: path.join(__dirname, dist),
    filename: '[name].[chunkhash].js',
    publicPath: '/'
  }
};
