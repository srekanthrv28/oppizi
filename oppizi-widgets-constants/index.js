import {oppiziRoutes} from './src/oppizi-routes';
import {userRoles} from './src/user-roles';

export {
  oppiziRoutes,
  userRoles
}
