// @flow
export const oppiziRoutes = {
  pricing: '/oppizi/pricing',
  cities: '/oppizi/cities'
}
