// @flow
import * as R from 'ramda'
const userStateKey = "user_state";

export const loadUserState = (): * => {
  try {
    const serializedState = localStorage.getItem(userStateKey);
    if (serializedState === null) {
      return undefined
    }
    return JSON.parse(serializedState);
  } catch(err) {
    console.error('Error while getting user state in local storage', err);
  }
}


export const saveUserState = (user_state: *): * => {
  try {
    const serializedState = R.toString(user_state);
    localStorage.setItem(userStateKey, serializedState);
  } catch(err) {
    console.error('Error while adding user state in local storage', err);
  }
}

export const deletUserState = () => {
  try {
    localStorage.clear(userStateKey);
  } catch(err) {
    console.error('Error while removing the user state', err);
  }
}
