export default function UpdateUserStateInStore(value, dispatch) {
  return dispatch({
    type: 'SET_USER', value
  })
}
