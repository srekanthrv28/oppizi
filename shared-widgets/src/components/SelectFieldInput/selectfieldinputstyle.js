// @flow
export const baseStyle : {
  fontSize: string,
  color: string
} = {
  fontSize: '16px',
  color: '#929090'
}

export const baseFloatingLabelFocusStyle : {
  color: string
} = {
  color: '#2BD5C6'
}

export const baseSelectedMenuItemStyle : {
  backgroundColor: string,
  color: string
} = {
  backgroundColor:'#5A3E68',
	color: '#fff'
}

export const labelStyle = {}
