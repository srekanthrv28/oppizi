// @flow
'use strict';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import React, {Component} from 'react';
import type { FieldProps } from 'redux-form';
import * as style from './selectfieldinputstyle';
import { Field } from 'redux-form';

type SelectFieldInputItem = {value: string | number, label: string | number};

type Props = {
  name: string,
  items: Array<SelectFieldInputItem>,
  fullWidth: boolean,
  style: typeof style.baseStyle,
  labelStyle: typeof style.labelStyle,
  floatingLabelFocusStyle: typeof style.baseFloatingLabelFocusStyle,
  selectedMenuItemStyle: typeof style.baseSelectedMenuItemStyle
} & FieldProps;

type DefaultProps = {
  style: typeof style.baseStyle,
  labelStyle: typeof style.labelStyle,
  floatingLabelFocusStyle: typeof style.baseFloatingLabelFocusStyle,
  selectedMenuItemStyle: typeof style.baseSelectedMenuItemStyle
};

function renderItems(items: Array<SelectFieldInputItem>): Array<MenuItem> {
	return items.map((item, index) => (
    <MenuItem
      key={index}
      value={item.value}
      primaryText={item.label}/>
  ));
}

const renderSelectFieldInput = (
  {
    input: {value, onChange, onBlur, ...inputProps},
    meta: { touched, error },
    items,
    labelStyle,
    fullWidth,
    floatingLabelFocusStyle,
    selectedMenuItemStyle,
    ...rest
  }: Props
) => {
	return (
    <SelectField
      floatingLabelFocusStyle = {floatingLabelFocusStyle}
      selectedMenuItemStyle = {selectedMenuItemStyle}
      labelStyle = {labelStyle}
      floatingLabelStyle={labelStyle}
      menuItemStyle={labelStyle}
      {...inputProps}
      value={value}
      fullWidth={fullWidth}
      onBlur= {() => onBlur(value)}
      onChange={(event, index, value) => onChange(value)}
      errorText={touched && error}
      children={renderItems(items)}
      {...rest}/>
	);
};

class SelectFieldInput extends Component<Props, void> {


	static defaultProps = {
		style: style.baseStyle,
		labelStyle: style.labelStyle,
		floatingLabelFocusStyle: style.baseFloatingLabelFocusStyle,
		selectedMenuItemStyle: style.baseSelectedMenuItemStyle
	}

	render() {
		const {
      name,
      ...rest
    } = this.props;

		return (
      <Field
        component={renderSelectFieldInput}
        name={name}
        {...rest}/>
		);
	}
}

export default SelectFieldInput;
