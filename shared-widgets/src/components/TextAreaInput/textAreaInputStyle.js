// @flow
export const baseStyle : {
  width: string,
  height: string,
  borderColor: string
} = {
  width: '100%',
  height: '150px',
  borderColor: '#eee'
};
