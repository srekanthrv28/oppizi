// @flow
import React, { Component } from 'react';
import { Field } from 'redux-form';
import type { FieldProps } from 'redux-form';
import * as style from './textAreaInputStyle';

type Props = {
	name: string,
	style: typeof style.baseStyle
} & FieldProps;

type DefaultProps = {
	style: typeof style.baseStyle
};

const renderTextArea = ({input, "meta": {touched, error}, ...rest}: Props): React$Element<*> => {
  const {onChange} = input;
	const errorText= touched && error;
  return (
		<div>
			<textarea
				style={rest.style}
				onChange={(e: *): * => onChange(e.target.value)}
				value={input.value}/>
				<p className="errorText">{errorText}</p>
		</div>
  );
};



class TextAreaInput extends Component<DefaultProps, Props, void> {

	props: Props;

	static defaultProps = {
		style: style.baseStyle
	};


	render(): React$Element<*> {
		const { name, ...rest} = this.props;

		return (
			<Field
				component={renderTextArea}
				name={name}
				{...rest}/>
		);
	}
}


export default TextAreaInput;
