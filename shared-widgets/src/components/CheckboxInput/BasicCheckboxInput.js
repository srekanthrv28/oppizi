// @flow
import React, { Component } from 'react';
import Checkbox from 'material-ui/Checkbox';
import * as style from './checkboxinputstyle';

type Props = {
	label: string,
  checked: boolean,
  onCheck: (event: *) => void,
	inputStyle: typeof style.baseInputStyle,
	iconStyle: typeof style.baseIconStyle,
	labelStyle: typeof style.baseLabelStyle,
	style: typeof style.baseStyle
};

type DefaultProps = {
	inputStyle: typeof style.baseInputStyle,
	labelStyle: typeof style.baseLabelStyle,
	iconStyle: typeof style.baseIconStyle,
	style: typeof style.baseStyle
};

class BasicCheckboxInput extends Component<DefaultProps, Props, void> {
  props: Props;

	static defaultProps = {
		inputStyle: style.baseInputStyle,
		labelStyle: style.baseLabelStyle,
		iconStyle: style.baseIconStyle,
		style: style.baseStyle
	};

	render(): React$Element<Checkbox> {

		const {
      label,
      inputStyle,
      iconStyle,
      labelStyle,
      style,
      checked,
      onCheck
    } = this.props;

		return(
      <Checkbox
				label={label}
				inputStyle={inputStyle}
				iconStyle={iconStyle}
				labelStyle={labelStyle}
				style={style}
				checked={checked}
				onCheck={onCheck}/>
		);
	}
}

export default BasicCheckboxInput;
