// @flow
export const baseIconStyle : {
  fill: string,
  marginRight: string
} = {
  fill: '#2BD5C6',
  marginRight: '8px'
};

export const baseLabelStyle : {
  color: string,
  fontSize: string,
  width: string,
  whiteSpace: string
} = {
  color: '#546371',
  fontSize: '16px',
  width: 'auto',
  whiteSpace: 'normal'
};

export const baseInputStyle : {
  width: string
} = {
  width: '24px'
};

export const baseStyle : {
  width: string,
  display: string,
  marginRight: string
} = {
  width: 'auto',
  display:'inline-flex',
  marginRight: '10px'
};
