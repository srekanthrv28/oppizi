// @flow
'use strict';
import BasicCheckboxInput from './BasicCheckboxInput';
import type { FieldProps } from 'redux-form';
import { Field } from 'redux-form';
import React, { Component } from 'react';

const renderCheckbox = ({input, meta: {touched, error}, ...restProps}: FieldProps): React$Element<*> => {
	return (
		<div>
			<BasicCheckboxInput
				label={restProps.label}
				checked={input.value ? true : false}
				onCheck={input.onChange}/>
			<div className="errorText">{touched && error}</div>
		</div>
	);
};

type Props = {
	name: string,
	label: string
} & FieldProps;

class CheckboxInput extends Component<void, Props, void> {

	props: Props;

	render(): React$Element<*> {
		const {name, ...rest} = this.props;
		return (
			<Field component={renderCheckbox}
				name={name}
				{...rest}/>
		);
	}
}
export default CheckboxInput;
