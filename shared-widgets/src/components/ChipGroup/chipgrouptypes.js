// @flow
export type ChipItemType = {id: number | string, label: string};
