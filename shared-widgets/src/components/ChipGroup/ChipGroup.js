// @flow
'use strict';
import ChipItem from '../ChipItem';
import React, { Component } from 'react';
import * as style from './chipgroupstyle';
import * as types from './chipgrouptypes'

type Props = {
  style: typeof style.baseStyle,
  chipList: Array<types.ChipItemType>,
  handleDelete: (id: string | number) => void
};

type DefaultProps = {
  style: typeof style.baseStyle
};

class ChipGroup extends Component<DefaultProps, Props, void>{

  props: Props;

  static defaultProps = {
    style: style.baseStyle
  }

  renderChip(chip: types.ChipItemType): React$Element<any>  {
    return <ChipItem
      key={chip.id}
      id = {chip.id}
      label = {chip.label}
      handleDelete={this.props.handleDelete}/>
  }

  renderGroup(chipList: Array<types.ChipItemType>): Array<React$Element<any>> {
    return chipList.map((chip: types.ChipItemType) => {
			return this.renderChip(chip);
    });
  }

  render() {
    const { style, chipList} = this.props;
    return(
      <div style={style}>
        {this.renderGroup(chipList)}
      </div>
    );
  }
}
export default ChipGroup;
