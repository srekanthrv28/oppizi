// @flow
export const baseStyle : {
  display: string,
  flexWrap: string
} = {
  display: 'flex',
  flexWrap: 'wrap'
};
