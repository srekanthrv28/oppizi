// @flow
'use strict';
import Chip from 'material-ui/Chip';
import React, { Component } from 'react';
import * as style from './chipitemstyle';

type Props = {
  handleDelete: (id: string | number) => void,
  style: typeof style.baseStyle,
  labelStyle: typeof style.labelStyle,
  label: string | number,
  id: number | string
};

type DefaultProps = {
  style: typeof style.baseStyle,
  labelStyle: typeof style.labelStyle
};

class ChipItem extends Component<DefaultProps, Props, void>{

  props: Props;

  static defaultProps = {
    style: style.baseStyle,
    labelStyle: style.labelStyle
  };

  render() {
    const { id, label, labelStyle, style, handleDelete} = this.props;

    return (
      <Chip
        key= {id}
        backgroundColor = {style.backgroundColor}
        labelColor = {style.labelColor}
        onRequestDelete = {() => handleDelete(id)}
        labelStyle={labelStyle}
        style={style}>
        {label}
      </Chip>
    );
  }
}
export default ChipItem;
