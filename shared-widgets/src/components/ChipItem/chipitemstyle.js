// @flow

export const baseStyle : {
  margin: string,
  color: string,
  borderRadius: string,
  backgroundColor: string,
  labelColor: string,
  maxWidth: string
} = {
  margin: '5px',
  color: '#fff',
  borderRadius: '0px',
  backgroundColor: '#5A3E68',
  labelColor: '#fff',
  maxWidth: '100%'
};

export const labelStyle : {
  maxWidth: string,
  textOverflow: string,
  whiteSpace: string,
  overflow: string
} = {
  maxWidth: '95%',
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  overflow: 'hidden'
}
