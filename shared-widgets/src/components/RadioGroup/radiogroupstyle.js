export const customIconStyle = {
  fill: '#2BD5C6',
  marginRight: '8px'
};

export const customLabelStyle = {
  color: '#929090',
  fontSize: '16px',
  width: 'auto'
};

export const radioButtonStyle = {
  width: 'auto',
  display: 'inline-flex',
  marginRight: '20px'
};
