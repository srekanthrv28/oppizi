// @flow
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import React, { Component } from 'react';
import { Field } from 'redux-form';
import type { FieldProps } from 'redux-form';
import * as style from './radiogroupstyle';

type RadioGroupItemType = {label: number | string, value: number | string, id: number | string};

type Props = {
	label: string,
	name: string,
	radioOptions: Array<RadioGroupItemType>,
	disabled: boolean,
	iconStyle: typeof style.customIconStyle,
	labelStyle: typeof style.customLabelStyle,
	style: typeof style.radioButtonStyle
} & FieldProps;

type DefaultProps = {
	iconStyle: typeof style.customIconStyle,
	labelStyle: typeof style.customLabelStyle,
	style: typeof style.radioButtonStyle
};

function renderRadioGroup(
	radioOptions: Array<RadioGroupItemType>,
	disabled: boolean,
	iconStyle: typeof style.customIconStyle,
	labelStyle: typeof style.customLabelStyle,
	style: typeof style.radioButtonStyle
): Array<RadioButton> {
	return radioOptions.map((radioOption: RadioGroupItemType): React$Element<RadioButton> => {
		return (
			<RadioButton
				label = {radioOption.label}
				value = {radioOption.value}
				key = {radioOption.id}
				disabled={disabled}
				iconStyle = {iconStyle}
				labelStyle = {labelStyle}
				style = {style}/>
		);
	});
}

const renderRadioButtons = (
	{
		input: {onChange, value, ...inputProps},
		meta: {touched, error},
		radioOptions,
		disabled,
		iconStyle,
		labelStyle,
		style,
		...rest
	}: Props
): React$Element<*> => {
	return (
		<div>
			<RadioButtonGroup
				{...inputProps}
				valueSelected={value}
				onChange={(event: *, value: *): void => onChange(value)}
				{... rest}>
				{renderRadioGroup(radioOptions, disabled, iconStyle, labelStyle, style)}
			</RadioButtonGroup>
			<div>
				{(touched && error) ? <span className="errorText">{error}</span> : ''}
			</div>
		</div>
	);
};

class RadioGroup extends Component<Props, void> {

	props: Props;

	static defaultProps = {
		iconStyle: style.customIconStyle,
		labelStyle: style.customLabelStyle,
		style: style.radioButtonStyle
	}

	render(): React$Element<*> {
		const { name, label, radioOptions, ...rest } = this.props;
		return (
			<div>
				<p>{label}</p>
				<Field
					component={renderRadioButtons}
					name={name}
					radioOptions={radioOptions}
					{...rest}/>
			</div>
		);
	}
}

export default RadioGroup;
