// @flow
import React, { Component } from 'react';
import { Field } from 'redux-form';
import type { FieldProps } from 'redux-form';
import TextField from 'material-ui/TextField';
import * as style from './textfieldinputstyle';

type Props = {
	name: string,
	type: string,
	hintText: string,
	floatingLabelText: string,
	fullWidth: boolean,
	underlineFocusStyle: typeof style.baseUnderlineStyle,
	style: typeof style.baseStyle,
	floatingLabelFocusStyle: typeof style.baseFloatingLabelFocusStyle,
	floatingLabelStyle: typeof style.labelStyle
} & FieldProps;

type DefaultProps = {
	underlineFocusStyle: typeof style.baseUnderlineStyle,
	style: typeof style.baseStyle,
	floatingLabelFocusStyle: typeof style.baseFloatingLabelFocusStyle,
	floatingLabelStyle: typeof style.labelStyle
};

const renderTextField = ({input, "meta": {touched, error}, ...rest}: Props): React$Element<TextField> =>
		<TextField
			errorText={touched && error}
			{...input}
			{...rest}/>

	;

class TextFieldInput extends Component<DefaultProps, Props, void> {

	props: Props;

	static defaultProps = {
		underlineFocusStyle: style.baseUnderlineStyle,
		style: style.baseStyle,
		floatingLabelFocusStyle: style.baseFloatingLabelFocusStyle,
		floatingLabelStyle: style.labelStyle
	};


	render(): React$Element<*> {
		const { name, ...rest} = this.props;

		return (
			<Field
				component={renderTextField}
				name={name}
				{...rest}/>
		);
	}
}


export default TextFieldInput;
