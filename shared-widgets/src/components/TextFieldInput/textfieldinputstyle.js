// @flow
export const baseUnderlineStyle : {
  borderColor: string
} = {"borderColor": '#2BD5C6'};

export const baseStyle : {
  fontSize: string,
  color: string
} = {
	fontSize: '16px',
	color: '#929090'
};

export const baseFloatingLabelFocusStyle : {
  color: string
} = {
	color: '#2BD5C6'
};

export const labelStyle = {}
