// @flow
import React, { Component } from 'react';
import DisplayAddress from '../GetDirections/DisplayAddress';
import GetDirections from '../GetDirections/GetDirections';
import * as R from 'ramda';

type Props = {
  address: {address1: string, address2: string, city: string, state: string, zip_code: string}
};

const googleMapBaseURL = 'https://www.google.com/maps/dir/?api=1&destination=';

class AddressAndDirection extends Component<Props, void> {

  render(): React$Element<*> {
    if(R.isNil(this.props.address)) return null;
    const {address} = this.props;
    const address1 = R.defaultTo('', address['address1']);
    const address2 = R.defaultTo('', address['address2']);
    const city = R.defaultTo('', address['city']);
    const state = R.defaultTo('', address['state']);
    const zip_code = R.defaultTo('', address['zip_code']);
    if(R.isEmpty(address1) && R.isEmpty(address2) && R.isEmpty(city) && R.isEmpty(state) && R.isEmpty(zip_code)) return null;

    return (
      <div style={styles.addressDirectionContainer}>
        <div style={styles.addressStyles}>
          <DisplayAddress address={address}/>
        </div>
        <div style={styles.directionStyle}>
          <GetDirections address={address}/>
        </div>
      </div>
    )
  }
}

export default AddressAndDirection;

const styles = {
  addressDirectionContainer: {
    clear : 'both',
    padding: '15px 0 0',
    overflow: 'hidden'
  },
  addressContainer: {
    width: '65%',
    float: 'left',
    display: 'inline-block',
    padding: '0 20px 0 0'
  },
  getDirectionStyle: {
    width: '35%',
    display: 'inline-block',
    float: 'left'
  },
  addressStyles: {
    display: 'inline-block',
    width: '65%',
    float: 'left'
  },
  directionStyle: {
    display: 'inline-block',
    width: '35%',
    float: 'left'
  }
}
