export const overlay = {
    position: 'absolute',
    left: '0',
    top: '0',
    bottom: '0',
    right: '0',
    filter: 'alpha(opacity=80)',
    zIndex: '9',
    background: 'rgba(0,0,0,0.3)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '200px',
    marginTop: '15px'
}

export const overLayText = {
  fontWeight: '600',
  color: '#b90523'
}
