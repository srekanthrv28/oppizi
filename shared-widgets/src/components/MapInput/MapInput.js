// @flow
import React, { Component } from 'react';
import * as R from 'ramda'
const redmarker = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
const greenMarker = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
const zoomLevel = 3;

type Props = {
  position: *,
};


class MapInput extends Component<Props, void> {

  componentDidMount() {
    this.map = new google.maps.Map(this.refs.map, {
      zoom: zoomLevel
    });
    this.updateMarkers();
  }

  updateMarkers()  {
    if(this.map) {
      const locations = this.props.positions;
        for (let i = 0; i < locations.length; i++) {
          const position = new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']);
          const markerIcon = (locations[i]['selected'] ? greenMarker : redmarker);
          const marker = new google.maps.Marker({
              position: position,
              map: this.map,
              icon: markerIcon
            });
          this.map.setCenter(position)
          google.maps.event.addListener(marker, 'click', ((marker, i) => {
            return () => {
              this.props.onCitySelected(locations[i]);
              if(marker.icon == greenMarker)
                marker.setIcon(redmarker);
              else
               marker.setIcon(greenMarker);
            }
          })(marker, i));
      }
    }
  }

  updateMap(position: *) {
    this.updateMarkers();
  }

  render(): React$Element<*> {
    this.updateMap(this.props.positions);
    return (
      <div style={styles.container}>
        <div ref="map" style={styles.mapStyle}/>
        <div ref="marker"/>
      </div>
    )
  }
}
export default MapInput;

const styles = {
  container: {
    position: 'relative'
  },
  mapStyle: {
    width: '100%',
    height: 400,
    border: '1px solid #979797'
  }
}
