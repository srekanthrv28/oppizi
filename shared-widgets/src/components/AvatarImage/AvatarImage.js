// @flow
'use strict';
import Avatar from 'material-ui/Avatar';
import React, { Component } from 'react';
import {defaultAvatarURL} from 'oppizi-widgets-constants';
import * as style from './avatarimagestyle';
import * as R from 'ramda'

type Props = {
  src: string,
  style: *,
  size: number,
  className: string,
  imagestyle: typeof style.imagestyle
};

type DefaultProps = {
  imagestyle: typeof style.imagestyle
};

class AvatarImage extends Component<Props, void>{

  static defaultProps = {
    imagestyle: style.imagestyle
  }

  getImageStyle = (): * =>  {
		const {style, imagestyle} = this.props;
		if(R.isNil(style)) {
			return {...imagestyle};
		}
		return {...style};
	}

  render(): React$Element<Avatar> {
    const {style, src, size, className, imagestyle} = this.props;
    return(
      <Avatar
        style={this.getImageStyle()}
        src={src || defaultAvatarURL.image_url}
        onError={(e)=>{e.target.src=defaultAvatarURL.image_url}}
        size={size}
        className={className}/>
    );
  }
}
export default AvatarImage;
