// @flow
import {oppiziCostCaluclateReducers} from 'oppizi-pricing-dashboard-display';
import { reducer as formReducer } from 'redux-form';
import * as R from 'ramda'

const reduxFormReducer = {"form": formReducer};

const webappReducers = R.mergeAll([
  oppiziCostCaluclateReducers,
  reduxFormReducer
]);

export default webappReducers;
