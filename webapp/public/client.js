// @flow
import 'react-dates/initialize';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components/Root';
import store from './store';

const app = document.getElementById('app');

ReactDOM.render(
	<MuiThemeProvider>
		<Root store={store} />
	</MuiThemeProvider>,
	app
);
