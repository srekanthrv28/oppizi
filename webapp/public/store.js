// @flow
import {
  applyMiddleware,
  createStore,
  combineReducers
} from 'redux';
import axios from 'axios';
import logger from 'redux-logger';
import promiseMiddleware from 'redux-promise-middleware';
import webappReducers from './webapp-reducers';
import thunk from 'redux-thunk';
import {loadUserState} from 'local-storage';
import * as R from 'ramda'

axios.defaults.baseURL = 'http://127.0.0.1:3000/';


const userState = R.defaultTo({}, loadUserState());
axios.defaults.headers.common['Authorization'] = R.defaultTo("", userState.token);

const checkAndResetAuthToken = (): * => (next: *): * => (action: *): * => {
  const userState = R.defaultTo({}, loadUserState());
  axios.defaults.headers.common['Authorization'] = R.defaultTo("", userState.token);
  return next(action);
}

const middleware = applyMiddleware(promiseMiddleware(), thunk, logger, checkAndResetAuthToken);
const appReducer = combineReducers(webappReducers);
const store = createStore(appReducer, middleware);


export default store;
