// @flow
export default {
  fr: {
    firstName: 'Prénom',
    lastName: 'Nom de famille',
    mobileNumber: 'Numéro de portable',
    email: 'Email',
    password: 'Mot de passe',
    confirmPassword: 'Confirmez le mot de passe',
    termsAndConditions: 'Termes et Conditions',
    iAgreeTo: 'Je suis d\'accord pour %{name}.'
  },
  en: {
    firstName: 'First Name',
    lastName: 'Last Name',
    mobileNumber: 'Mobile Number',
    email: 'Email',
    password: 'Password',
    confirmPassword: 'Confirm Password',
    termsAndConditions: 'Terms & Conditions',
    iAgreeTo: 'I Agree to %{name}.'
  }
}
