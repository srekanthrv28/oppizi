// @flow
import React from 'react';
import {
	Route,
  Redirect
} from 'react-router-dom';
import {
	oppiziRoutes
} from 'oppizi-widgets-constants';

const getRedirectPath = (): string => {
		return oppiziRoutes.pricing;
}

const PublicRoute = ({ component: Component, isLoggedIn, ...rest }: *): * => {
  return (
    <Route {...rest} render={(props: *): * => (
      !isLoggedIn ? (
        <Component {...props}/>
      ) : (
        <Redirect push to={{
          pathname: getRedirectPath(),
          state: { from: props.location }
        }}/>
      )
    )}/>
  );
}

export default PublicRoute;
