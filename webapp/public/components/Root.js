// @flow
import WebappRouter from './WebappRouter';
import { Provider } from 'react-redux';
import React, {Component} from 'react';
import I18nProvider from './I18nProvider';

const locale = 'en';

type Props = {
  store: *
};

class Root extends Component<void, Props, void> {

  props: Props;

  render(): React$Element<*> {
		return (
			<Provider store={this.props.store}>
        <I18nProvider locale={locale}>
          <WebappRouter/>
        </I18nProvider>
			</Provider>
		);
	}
}
export default Root;
