// @flow
import React, { Component } from 'react';
import {
	Switch,
	BrowserRouter,
	Route,
	Redirect
} from 'react-router-dom';
import { oppiziRoutes,  userRoles} from 'oppizi-widgets-constants';
import { DashboardRoot } from 'oppizi-pricing-dashboard';
import {connect} from 'react-redux';
import type {Store} from 'redux';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

type Props = { };

@connect((store: Store): * => ({
  user: store.user
}))
class WebappRouter extends Component<void, Props, void> {

	props: Props;

  render(): React$Element<*> {
		return (
			<BrowserRouter>
				<Switch>
					<PrivateRoute path={oppiziRoutes.pricing} component={DashboardRoot} role={[userRoles.oppiziAdmin]} userRole={'oppiziAdmin'} isLoggedIn={false}/>
					<Route path="/*" render={(): * => <Redirect to={oppiziRoutes.root}/>}/>
				</Switch>
			</BrowserRouter>
		);
  }
}
export default WebappRouter;
