// @flow
import React, {Component} from 'react';
import nurtureCo_logo from './Logo_Product.png';

class OppiziLogo extends Component<void, void> {

  render(): React$Element<*> {
    return (
      <img style={styles.container} src={nurtureCo_logo}/>
    );
  }
}

export default OppiziLogo;

const styles = {
  container: {
    width: '30px',
    margin: '12px 0 0'
  }
}
