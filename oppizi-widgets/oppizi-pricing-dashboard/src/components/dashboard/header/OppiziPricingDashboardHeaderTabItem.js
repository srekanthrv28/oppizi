// @flow
import React, {Component} from 'react';
import {Link} from 'react-router-dom';

type Props = {
  label: string | React$Element<*>,
  tabItem: React$Element<*>,
  isSelected: boolean
};

class OppiziPricingDashboardHeaderTabItem extends Component<void, Props, void> {

  props: Props;

  getLinkStyle = (): * => {
    const {isSelected} = this.props;
    if(isSelected) {
      return {...styles.linkContainer, backgroundColor: '#2bd5c6'};
    }
    return styles.linkContainer;
  }

  render(): React$Element<*> {
    const {label, tabItem} = this.props;
    return (
      <li >
        <Link
          to={tabItem}
          style={this.getLinkStyle()}>
          {label}
        </Link>
      </li>
    );
  }
}
export default OppiziPricingDashboardHeaderTabItem;

const styles = {
  linkContainer: {
    padding: '15px',
    backgroundColor: 'transparent',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: '14px',
    cursor: 'pointer'
  }
}
