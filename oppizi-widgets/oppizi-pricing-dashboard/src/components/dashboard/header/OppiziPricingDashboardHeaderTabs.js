// @flow
import React, {Component} from 'react';
import OppiziPricingDashboardHeaderTabItem from './OppiziPricingDashboardHeaderTabItem';
import * as R from 'ramda'

type Props = {
  headerItems: Array<string>,
  currentSelectedTabIndex: number
};

class OppiziPricingDashboardHeaderTabs extends Component<void, Props, void> {

  renderHeaderItems = (): Array<*> => {
    const {headerItems, currentSelectedTabIndex} = this.props;
    const headerItemsList = R.keys(headerItems).map((headerItem: string, index: number): React$Element<*> => {
      return (
        <OppiziPricingDashboardHeaderTabItem
          key={index}
          isSelected={currentSelectedTabIndex === index}
          tabItem={headerItems[headerItem]}
          label={headerItem}/>
      )
    });
    return headerItemsList;
  }

  render(): React$Element<*> {
    return (
      <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul className="nav navbar-nav" style={styles.container}>
          {this.renderHeaderItems()}
        </ul>
      </div>
    );
  }
}
export default OppiziPricingDashboardHeaderTabs;

const styles = {
  container: {
    marginBottom: '0px'
  }
}
