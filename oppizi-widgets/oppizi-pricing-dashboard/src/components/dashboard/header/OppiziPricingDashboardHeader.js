// @flow
import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import {OppiziPricingDashboardDisplay} from 'oppizi-pricing-dashboard-display';
import OppiziPricingDashboardHeaderTabs from './OppiziPricingDashboardHeaderTabs';
import {oppiziRoutes} from 'oppizi-widgets-constants';
import {OppiziLogo} from 'oppizi-shared-widgets';

type Props = {
  pathname: string,
  path: string,
  dispatch: *,
  firstName: string
};

type State = {
  currentSelectedTabIndex: number,
  currentSelectedTabName: string
};


const headerItems = {
  'Pricing': oppiziRoutes.pricing
};

class OppiziPricingDashboardHeader extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      currentSelectedTabIndex: -1,
      currentSelectedTabName: null,
    };
  }

  getContent = (): * => {
    const {dispatch, firstName} = this.props;
    return (
      <div>
        <nav style={styles.nav} className="navbar navbar-default">
          <div className='container-fluid'>
            <div className='row'>
              <div className='col-sm-10 col-xs-9'>
                <a className='hidden-sm hidden-xs' href='javascript:void(0)' style={styles.logoContainer}><OppiziLogo/></a>
                <OppiziPricingDashboardHeaderTabs headerItems={headerItems} currentSelectedTabIndex={this.state.currentSelectedTabIndex}/>
              </div>
            </div>
          </div>
        </nav>
        <div className='container-fluid xs-p-0'>
          <Route path={oppiziRoutes.pricing} component={OppiziPricingDashboardDisplay}/>
         </div>
      </div>
    );
  }

  render(): React$Element<*> {
    return (
      <div>
        {this.getContent()}
      </div>
    );
  }
}
export default OppiziPricingDashboardHeader;

const styles = {
  nav: {
    backgroundColor: '#5A3E68',
    border: 'none',
    borderRadius: '0px',
    minHeight: 'auto !important'
  },
  logoContainer: {
    float: 'left'
  }
}
