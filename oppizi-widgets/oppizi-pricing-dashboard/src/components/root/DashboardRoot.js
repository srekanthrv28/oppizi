// @flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import OppiziPricingDashboardHeader from '../dashboard/header/OppiziPricingDashboardHeader';

type Props = {
  user: {firstName: string},
  serverResponse: {message: string, messageType: string},
  dispatch: *,
  location: {pathname: string},
  match: {path: string}
};

class DashboardRoot extends Component<void, Props, void> {

  props: Props;

  render(): React$Element<*> {
      return (
      <div>
        <OppiziPricingDashboardHeader/>
      </div>
    );
  }
}
export default DashboardRoot;
