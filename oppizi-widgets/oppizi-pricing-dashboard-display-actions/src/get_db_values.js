// @flow
import axios from 'axios';

const url = '/get_all_data/';

async function getDbValues(values: *, dispatch: *): * {
  await dispatch({
      type: 'FETCH_ALL_DATA',
      payload: axios.get(url)
    });
}

export default getDbValues;
