// @flow
import React, {Component} from 'react';
import CitiListItem from './CitiListItem';
import FlatButton from 'material-ui/FlatButton';
import * as R from 'ramda';
import css from '../style.css';
import {connect} from 'react-redux';
import { change } from 'redux-form';

@connect((store: Store): * => ({
  dispatch: store.dispatch,
  data: store.costCaluclate
}))

class CitiesList extends Component<void, void, void> {

  onCitySelected = (city: string) => {
    const {dispatch} = this.props;
    if(city) {
      dispatch(change('OppiziPricingDashboardDisplayForm', 'city', city.name))
      dispatch({type: 'UPDATE_SLECTED_CITY_VALUE', payload: city});
    }
  }
  renderCitiesList = (): Array<*> => {
    const {citiesList} = this.props.data;
    if(R.isNil(citiesList) || R.isEmpty(citiesList)) return null;
      const caregiverResult = citiesList.map((city: *, index: number): * => {
        return (
          <div key={index} style={styles.listViewStyle} onClick={(): void => this.onCitySelected(city)} className={css.bestMatchesListStyle}>
          <CitiListItem
            cityDetails={city}
            setBackgrounColour={city.selected}/>
          </div>
        );
      });
    return caregiverResult;
  }

  render(): React$Element<*> {
    return (
      <div style={styles.ulStyle}>
        <div style = {styles.container}>
          {this.renderCitiesList()}
        </div>
      </div>
    );
  }
}

export default CitiesList;

const styles = {
  loadMoreContainer:{
    padding: '15px'
  },
  label:{
    color: '#929090'
  },
  container:{
    display: 'table',
    margin: '0 auto'
  },
  listViewStyle:{
    padding: '0px',
    width: '140px',
    float:'left'
  },
  ulStyle:{
    width:'100%'
  }
}
