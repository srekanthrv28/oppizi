// @flow
import React, {Component} from 'react';
import Checkbox from 'material-ui/Checkbox';
import {AvatarImage} from 'shared-widgets';
import css from '../style.css';


type Props = {};

class CitiListItem extends Component<void, Props, void> {

  props: Props;

  getSelectedContainerStyle = (): * => {

   const {setBackgrounColour} = this.props;
   if (setBackgrounColour) {
     return {...styles.innerContainer, backgroundColor: 'rgba(184, 223, 134, 0.18)' , boxShadow: '0 2px 12px 2px rgba(89, 62, 104, 0.35)', transform: 'scale(1.05,1.05)', transition: 'all 0.2s ease'};
   } else {
     return {...styles.innerContainer};
   }
 }

  render(): React$Element<*> {
    const {cityDetails} = this.props;
    return (
      <div style={styles.listContainer} >
        <div className={css.citiInfoWrapper} style={this.getSelectedContainerStyle()}>
          <div style={styles.avatarContainer}>
            <AvatarImage src={cityDetails.pic_url} size={40}/>
          </div>
          <div style={styles.clientInfo}>
            <h4 style={styles.title}> {cityDetails.name} </h4>
            <div> {cityDetails.country} </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CitiListItem;

const styles = {
  innerContainer:{
    display: 'flex',
    flexDirection: 'row',
    background: '#FFFFFF',
    boxShadow: '0 2px 2px 0 rgba(0,0,0,0.13)',
    position: 'relative',
    padding: '10px',
    cursor: 'pointer'
  },
  clientInfo:{
    paddingLeft: '7px',
    overflow: 'hidden'
  },
  title:{
    fontSize: '11px',
    color: '#2C3238',
    marginBottom: '5px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  listContainer:{
    padding:'0 5px'
  }
}
