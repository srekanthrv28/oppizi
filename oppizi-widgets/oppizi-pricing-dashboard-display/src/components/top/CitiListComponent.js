// @flow
import React, {Component} from 'react';
import HelpIcon from 'material-ui/svg-icons/action/help';
import CitiesList from './CitiesList';
import css from '../style.css';

class CitiListComponent extends Component<void, void, void> {

  render(): React$Element<*> {
    return (
      <div className={css.scheduleStatsContainer}>
        <div className={'row'}>
        <h3 style={styles.title}>{'Cost Estimator'} <HelpIcon style={styles.icon} /></h3>
        <CitiesList />
        </div>
      </div>
    );
  }
}

export default CitiListComponent;

const styles = {
  container:{
    margin: '20px 0px'
  },
  title:{
    fontSize: '24px',
    color: '#171617',
    letterSpacing: '0.52px',
    lineHeight: '38px',
    fontWeight: '300'
  },
  icon:{
    fill: '#5A3E68',
    width: '15px',
    height: '15px',
    position: 'relative',
    top: '-5px'
  }
}
