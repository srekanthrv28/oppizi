export const durationList = [
	{
		label: "2 Weeks",
		value: '2',
		id: '2'
	},
	{
		label: "4 Weeks",
		value: '4',
		id: '4'
	},
  {
		label: "6 Weeks",
		value: '6',
		id: '6'
	},
  {
		label: "8 Weeks",
		value: '8',
		id: '8'
	}
];
