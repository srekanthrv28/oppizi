//@flow
import React, {Component} from 'react';
import Slider from 'material-ui/Slider';
import {connect} from 'react-redux';
import { change } from 'redux-form';
import {purposeSelectedList} from './purpose-selected-values-list';
import * as R from 'ramda';


@connect((store: Store): * => ({
  dispatch: store.dispatch
}))

export default class RangeSelectorComponent extends Component {
  state = {
    sliderValue: 0,
  };

  handleSlider = (event: *, value: *) => {
    const {dispatch} = this.props;
    console.log("value", value, purposeSelectedList);
    const purposeData = R.find(R.propEq('value', value))(purposeSelectedList);
    dispatch(change('OppiziPricingDashboardDisplayForm', 'purpose', purposeData.type));
    dispatch({type:'PURPOSE_VALUE_UPDATE', payload: purposeData.type});
  };

  render(): * {
    return (
      <div>
        <label style={styles.labelStyle}>{'Purpose'}</label>
          <Slider
            min={0}
            max={100}
            step={33}
            value={this.state.sliderValue}
            onChange={this.handleSlider}
            sliderStyle = {styles.sliderStyle}
          />
      </div>
    );
  }
}

const styles = {
  labelStyle: {
    color: '#2bd5c6',
    fontSize: '14px',
    fontWeight: '400',
    display: 'block'
  },
  sliderStyle:{
    margin: '20px 0'
  }
}
