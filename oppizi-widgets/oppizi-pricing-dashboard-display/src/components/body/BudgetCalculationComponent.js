// @flow
import * as React from 'react';
import {Card, CardText} from 'material-ui/Card';
import {connect} from 'react-redux';
import css from '../style.css';
import * as R from 'ramda';
import {TextFieldInput, SelectFieldInput} from 'shared-widgets';
import { change } from 'redux-form';

type Props = {
  budget: number,
  signups: number,
  cps: number
};

@connect((store: Store): * => ({
  dispatch: store.dispatch,
  data: store.costCaluclate
}))

class BudgetCalculationComponent extends React.Component<Props, void> {

  getFixedValue = (val: *) => {
    if(R.isNil(val)) return '?';
    return 'AU$' + val.toFixed(2);
  }

  getFixedSignupValue = (val: *) => {
    if(R.isNil(val)) return '?';
    return val.toFixed(2);
  }

  getIndustries = () => {
    const {convertionList} = this.props.data;
    return convertionList.map((industry: *) => {
      return (
        {
          label: `${industry['name']}`,
          value: industry['name']
        }
      )
    });
  }

  handleChange = (event, value) => {
    const {dispatch} = this.props;
    if(value) {
      dispatch({type: 'UPDATE_INDUSTRY_VALUE', payload: value});
    }
  };

  render(): React.Node {
    const {budget, signups, cps} = this.props.data;
    return (
      <div style={styles.headerContainer}>
        <Card>
          <CardText expandable={false}>
            <div className={css.headerContainer}>
              <div className={css.totalBudgetContainer}>
                <div className={css.totalBudget}>
                  <h4>{'Budget:'}</h4>
                  <span>{`${this.getFixedValue(budget)}`} </span>
                </div>
              </div>

              <div className={css.selectIntervalContainer}>

                <SelectFieldInput
                  iconStyle={{top: '-15px', right: '60px'}}
                  hintStyle={{fontSize: '12px', bottom: '26px'}}
                  hintText={'Select You Industry'}
                  labelStyle={{fontSize: '12px', top: '-17px'}}
                  items={this.getIndustries()}
                  onChange={this.handleChange}
                  underlineStyle	= {{border:'none'}}
                  name={'industry'}/>

                <div className={css.signupsandcpsContainer}>
                  <div className={css.signupStyle}>
                    <h3 className={css.textStyle}>{`Signups: ${this.getFixedSignupValue(signups)}`}</h3>
                  </div>

                  <div className={css.cpsStyle}>
                    <h3 className={css.textStyle}>{`CPS: ${this.getFixedValue(cps)}`}</h3>
                  </div>

                </div>
              </div>
            </div>
          </CardText>
        </Card>
      </div>
    );
  }
}
export default BudgetCalculationComponent;

const styles ={
  headerContainer: {
    marginBottom : '50px'
  }
}
