// @flow
import React, {Component} from 'react';
import { RadioGroup } from 'shared-widgets';
import {durationList} from './duration-constant-list';
import {connect} from 'react-redux';
import { change } from 'redux-form';
import * as R from 'ramda';


@connect((store: Store): * => ({
  dispatch: store.dispatch
}))
class DurationSelectorComponent extends Component<Props, void> {

	getContainerStyle = (): * => {
     return styles.container;
  }

	changeDuration = (value: number) => {
    const {dispatch} = this.props;
		if(value && !R.isEmpty(value)){
      dispatch(change('OppiziPricingDashboardDisplayForm', 'duration', value))
      dispatch({type:'UPDATE_SELECTED_DURATION_VALUE', payload: value[0]});
    }
  };

  render(): React$Element<*> {
    return (
      <div style={this.getContainerStyle()}>
				<label style={styles.label}>{'Duration'}</label>
        <RadioGroup
				  onChange={this.changeDuration}
          radioOptions={durationList}
          name={'duration'}/>
      </div>
    );
  }
}

export default DurationSelectorComponent;

const styles = {
	container: {
		display: 'block',
		marginBottom: '50px'
	},
	label: {
    color: '#2bd5c6',
    fontSize: '14px',
    fontWeight: 400,
    display: 'block',
    marginBottom: '16px'
  }
}
