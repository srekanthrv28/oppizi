// @flow
import React, {Component} from 'react';
import {MapInput} from 'shared-widgets';
import {connect} from 'react-redux';
import { change } from 'redux-form';



type Props = {
  positions: Array<string>
};

@connect((store: Store): * => ({
  dispatch: store.dispatch,
  data: store.costCaluclate
}))

class LocationMapComponent extends Component<Props, void, void> {

  props: Props;

  onCitySelected = (city: string) => {
    const {dispatch} = this.props;
    if(city) {
      dispatch(change('OppiziPricingDashboardDisplayForm', 'city', city.name))
      dispatch({type: 'UPDATE_SLECTED_CITY_VALUE', payload: city});
    }
  }

  render(): React$Element<*> {
    const {data: {citiesList}, dispatch} = this.props;
    return (
      <div className={"col-sm-6"}>
        <MapInput positions={citiesList} dispatch={dispatch} onCitySelected={this.onCitySelected}/>
      </div>
    )
  }
}
export default LocationMapComponent;
