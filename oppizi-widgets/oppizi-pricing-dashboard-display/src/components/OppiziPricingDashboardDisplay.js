//@flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';
import type { FormProps } from 'redux-form';
import CitiListComponent from './top/CitiListComponent';
import DurationSelectorComponent from './body/DurationSelectorComponent';
import LocationMapComponent from './body/LocationMapComponent';
import BudgetCalculationComponent from './body/BudgetCalculationComponent';
import RangeSelectorComponent from './body/RangeSelectorComponent';
import ContactUsForm from './buttom/ContactUsForm';
import * as R from 'ramda';

type Props = {} & FormProps;

type State = { };

@connect((store: Store): * => ({
  dispatch: store.dispatch
}))

class OppiziPricingDashboardDisplayForm extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
    };
  }

  onSubmit(values: *, dispatch: *): * {
   console.log("all entered values by user", values);  
   return "";
  }

  render(): React$Element<*> {
    const {handleSubmit} = this.props;
    return(
      <div>
        <form onSubmit={handleSubmit(this.onSubmit)}>
          <CitiListComponent/>
          <div className={'row'}>
            <LocationMapComponent/>
            <div className={'col-md-6'} >
              <BudgetCalculationComponent/>
              <DurationSelectorComponent/>
              <RangeSelectorComponent/>
            </div>
          </div>
          <div className={'row'}>
            <div className={'col-md-12'}>
              <div style={styles.container}>
                <ContactUsForm />
                <div className="text-center">
                    <div style={styles.button}>
                        <button type="submit" style={styles.btnStyle}>
                          {'Submit'}
                       </button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}


const createForm = reduxForm({
  'destroyOnUnmount': true,
  'enableReinitialize': true,
  'initialValues': {'duration': "2"},
  'form': 'MyFrom'
});

const OppiziPricingDashboardDisplay = createForm(OppiziPricingDashboardDisplayForm);

export default OppiziPricingDashboardDisplay;

const styles = {
  contentContainer: {
    width: '100%',
    overflow: 'auto'
  },
  button: {
    overflow: 'hidden',
    marginTop : '30px'
  },
  btnStyle: {
    border: 'none',
    color: '#ffffff',
    padding: '8px 24px',
    background: '#2bd5c6',
    fontSize:'16px',
    borderRadius: '4px',
    fontWeight : '600'
  }
}
