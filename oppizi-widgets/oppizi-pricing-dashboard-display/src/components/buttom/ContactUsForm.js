// @flow
import React, {Component} from 'react';
import {TextFieldInput, Translate } from 'shared-widgets';
import {fields} from './user-contact-us-form-fields';

class ContactUsForm extends Component<void, void> {

  render(): React$Element<*> {
    return (
      <div className={'clearfix'}>
        <div className={'col-md-12'}>
          <h3 className={'text-center'}>{'Contact US'}</h3>
        </div>
        <div className={'col-md-6 col-sm-12'}>
          <TextFieldInput
            type="text"
            hintText={'Name'}
            floatingLabelText={'Name'}
            fullWidth={true}
            name={fields.firstName}/>
        </div>
        <div className={'col-md-6 col-sm-12'}>
          <TextFieldInput
            type="text"
            hintText={'Organization'}
            floatingLabelText={'Organization'}
            fullWidth={true}
            style={styles.formFields}
            name={fields.lastName}/>
        </div>
        <div className={'col-md-6 col-sm-12'}>
          <TextFieldInput
            type="number"
            hintText={'MobileNumber'}
            floatingLabelText={'MobileNumber'}
            fullWidth={true}
            style={styles.formFields}
            name={fields.mobileNumber}/>
        </div>
        <div className={'col-md-6 col-sm-12'}>
          <TextFieldInput
            type="email"
            hintText={'Email'}
            floatingLabelText={'Email'}
            fullWidth={true}
            style={styles.formFields}
            name={fields.email}/>
          </div>
      </div>
    );
  }
}

export default ContactUsForm;

const styles = {
	checkBoxContainer: {
		position: 'relative',
		marginBottom: '10px'
	},
	checkBox: {
		position: 'absolute',
	},
	labelStyle: {
		position: 'relative',
		left: '30px'
	}
}
