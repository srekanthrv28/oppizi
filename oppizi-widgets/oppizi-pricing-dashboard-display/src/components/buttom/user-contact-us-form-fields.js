// @flow

export const fields =  {
  'firstName': 'first_name',
  'lastName': 'last_name',
  'mobileNumber': 'mobile_number',
  'email': 'email'
}
