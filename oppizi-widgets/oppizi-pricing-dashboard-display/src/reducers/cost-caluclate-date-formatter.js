// @flow
import * as R from 'ramda'

function costCaluclateFormatter(state: *): * {
  const citiesList = R.map(R.curry(updateselectedValue)(state), state.citiesList);
  const combinedBudget  = R.reduce((acc: Number, city: string): mixed => acc + city.budget, 0 , citiesList);
  const combinedflyersCount  = R.reduce((acc: Number, city: string): mixed => acc + city.flyersCount, 0 , citiesList);
  if(state.selctedIndustry) {
    const updated_state = {...state, citiesList: citiesList, budget: combinedBudget};
    return updateSignupsAndCPSValue(updated_state, combinedflyersCount);
  }
  return {...state, citiesList: citiesList, budget: combinedBudget};
}

const isSelected = (city: string): boolean => city.selected;

const updateselectedValue = (state: *, cityData: *): * => {
  if(cityData.selected) {
    const purposeData = R.find(R.propEq('city', cityData.name))(state.purposeList);
    const purposeValue = parseInt(purposeData[state.selectedPurpose]);
    const flyersCount =  purposeValue * parseInt(state.selctedDuration);
    const flyersPerMission = R.find(R.propEq('name', cityData.name))(state.distributionList);
    const printingCostDate = R.find(R.curry(findCostValue)(flyersCount))(state.printingCostList);
    const printingCost = parseInt(printingCostDate.printing_cost);
    const flyers_per_mission = parseInt(flyersPerMission.flyers_per_mission);
    const cost_per_mission = parseInt(flyersPerMission.cost_per_mission);
    const budjet = ((flyersCount / flyers_per_mission) * cost_per_mission) + (flyersCount * printingCost);
    console.log("flyersCount", flyersCount);
    return {...cityData, budget: budjet, flyersCount: flyersCount}
  }
  return {...cityData, budget: 0, flyersCount: 0};
}

const updateSignupsAndCPSValue = (state: *, flyersCount: Number): * => {
  const selctedIndustry = state.selctedIndustry;
  const contvertionData = R.find(R.propEq('name', selctedIndustry))(state.convertionList);
  const contvertion_rate = parseFloat(contvertionData.contvertion_rate);
  const signups = flyersCount * (contvertion_rate/100);
  const CPS = (state.budget/signups);
  return {...state, signups: signups, cps: CPS};
}

const findCostValue = (flyersCount: Number, distribution: *): boolean => {
  const flyers_order_from = parseInt(distribution.flyers_order_from);
  const flyers_order_to = parseInt(distribution.flyers_order_to);
  return (flyersCount > flyers_order_from && flyersCount <= flyers_order_to );
}

export default costCaluclateFormatter;

// const test = (((15000*2) / 600) * 105 ) + ((15000*2) *  0.0897);
// 2   - Weeks
// 15000 - Flyers per week for Sydney.
// 600 - Flyers per mission.
// 105 - mission cost.
// 0.0897 - flyer printing cost up to 36631 prints.
