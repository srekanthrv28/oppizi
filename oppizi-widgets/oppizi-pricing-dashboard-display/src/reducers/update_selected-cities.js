// @flow
import * as R from 'ramda'

function updateSelectedCities(state: *, selectedCity: string): * {
  const citiesList = R.map(R.curry(updateselectedValue)(selectedCity), state.citiesList);
  return {...state, citiesList: citiesList};
}

const updateselectedValue = (selectedCity: string, cityData: *): * => {
  if(R.equals(cityData.name, selectedCity.name)) {
    return {...cityData, selected: !cityData.selected}
  }
  return cityData;
}

export default updateSelectedCities;
