// @flow
import costCaluclateFormatter from './cost-caluclate-date-formatter';
import updateSelectedCities from './update_selected-cities';
import {defaultCitiesList} from './default/cities-list';
import {defaultPurposeList} from './default/purpose-list';
import {defaultPrintingCostList} from './default/priting-cost-list';
import {defaultDistributionList} from './default/distribution-list';
import {defaultConvertionList} from './default/convertion-list';
const defaultPurpose = "test";

const initialState =  {
  purposeList: defaultPurposeList,
  printingCostList:defaultPrintingCostList,
  distributionList: defaultDistributionList,
  citiesList: defaultCitiesList,
  convertionList: defaultConvertionList,
  selctedDuration: 2,
  selectedPurpose: defaultPurpose,
  selctedIndustry: null,
  budget: null,
  signups: null,
  cps: null,
}

function costCaluclateReducer(state: * = initialState, action: *): * {
  switch (action.type) {
    case 'UPDATE_INDUSTRY_VALUE': {
      const serverResp = action.payload;
      const updated_state = {...state, selctedIndustry: serverResp};
      state = costCaluclateFormatter(updated_state)
      break;
    }
  case 'UPDATE_SLECTED_CITY_VALUE': {
    const serverResp = action.payload;
    const updated_state = updateSelectedCities(state, serverResp)
    state = costCaluclateFormatter(updated_state, serverResp)
    break;
  }
  case 'UPDATE_SELECTED_DURATION_VALUE': {
    const serverResp = action.payload;
    const updated_state = {...state, selctedDuration: serverResp}
    state = costCaluclateFormatter(updated_state, serverResp)
    break;
  }
  case 'PURPOSE_VALUE_UPDATE': {
    const serverResp = action.payload;
    const updated_state = {...state, selectedPurpose: serverResp}
    state = costCaluclateFormatter(updated_state, serverResp)
    break;
  }
  default:
      break;
  }
  return state;
}

export default costCaluclateReducer;
