//@flow

export const defaultPrintingCostList = [
  {"flyers_order_from":0,"flyers_order_to":6000,"printing_cost":0.16},
  {"flyers_order_from":6000,"flyers_order_to":12600,"printing_cost":0.13},
  {"flyers_order_from":12600,"flyers_order_to":19860,"printing_cost":0.11},
  {"flyers_order_from":19860,"flyers_order_to":27846,"printing_cost":"0.10"},
  {"flyers_order_from":27846,"flyers_order_to":36631,"printing_cost":"0.09"},
  {"flyers_order_from":36631,"flyers_order_to":46294,"printing_cost":"0.08"},
  {"flyers_order_from":46294,"flyers_order_to":56923,"printing_cost":"0.08"},
  {"flyers_order_from":56923,"flyers_order_to":68615,"printing_cost":"0.08"},
  {"flyers_order_from":68615,"flyers_order_to":81477,"printing_cost":"0.07"},
  {"flyers_order_from":81477,"flyers_order_to":95625,"printing_cost":"0.07"},
  {"flyers_order_from":95625,"flyers_order_to":111187,"printing_cost":"0.07"},
  {"flyers_order_from":111187,"flyers_order_to":128306,"printing_cost":"0.07"},
  {"flyers_order_from":128306,"flyers_order_to":147136,"printing_cost":"0.07"},
  {"flyers_order_from":147136,"flyers_order_to":167850,"printing_cost":"0.06"},
  {"flyers_order_from":167850,"flyers_order_to":190635,"printing_cost":0.06},
  {"flyers_order_from":190635,"flyers_order_to":"215698","printing_cost":"0.06"},
  {"flyers_order_from":215698,"flyers_order_to":"243268","printing_cost":"0.06"},
  {"flyers_order_from":243268,"flyers_order_to":"273595","printing_cost":"0.06"},
  {"flyers_order_from":273595,"flyers_order_to":"306955","printing_cost":"0.06"},
  {"flyers_order_from":"306955","flyers_order_to":"343650","printing_cost":"0.05"},
  {"flyers_order_from":"343650","flyers_order_to":"384015","printing_cost":"0.05"},
  {"flyers_order_from":"384015","flyers_order_to":"428416","printing_cost":"0.05"},
  {"flyers_order_from":"428416","flyers_order_to":"477258","printing_cost":"0.05"},
  {"flyers_order_from":"477258","flyers_order_to":"530984","printing_cost":"0.05"},
  {"flyers_order_from":"530984","flyers_order_to":"590082","printing_cost":"0.05"},
  {"flyers_order_from":"590082","flyers_order_to":"6,55,091","printing_cost":"0.04"},
  {"flyers_order_from":"655091","flyers_order_to":"7,26,600","printing_cost":"0.04"},
  {"flyers_order_from":"726600","flyers_order_to":"8,05,260","printing_cost":"0.04"},
  {"flyers_order_from":"805260","flyers_order_to":"8,91,786","printing_cost":"0.04"},
  {"flyers_order_from":"891786","flyers_order_to":"9,86,964","printing_cost":"0.04"},
  {"flyers_order_from":"1000000","flyers_order_to":"10,00,00,000","printing_cost":"0.04"}
]
