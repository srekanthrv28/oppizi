//@flow

export const defaultPurposeList = [
  {"city":"Sydney","test":"15000","validate":"20000","growth":"40000","scale-up":"60000"},
  {"city":"Melbourne","test":"15000","validate":"20000","growth":"35000","scale-up":"52500"},
  {"city":"Brisbane","test":"10000","validate":"15000","growth":"25000","scale-up":"37500"},
  {"city":"Perth","test":"10000","validate":"15000","growth":"20000","scale-up":"30000"},
  {"city":"Adelaide","test":"10000","validate":"15000","growth":"20000","scale-up":"30000"},
  {"city":"Auckland","test":"15000","validate":"20000","growth":"30000","scale-up":"45000"},
  {"city":"Canberra","test":"5000","validate":"6000","growth":"7500","scale-up":"11250"},
  {"city":"Newcastle","test":"5000","validate":"6000","growth":"7500","scale-up":"11250"},
  {"city":"Christchurch","test":"5000","validate":"6000","growth":"7500","scale-up":"11250"},
  {"city":"Wellington","test":"10000","validate":"11000","growth":"12500","scale-up":"18750"}
];
