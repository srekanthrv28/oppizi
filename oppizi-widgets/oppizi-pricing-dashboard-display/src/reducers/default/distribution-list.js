//@flow

export const defaultDistributionList = [
    {"name":"Sydney","flyers_per_mission":600,"cost_per_mission":105},
    {"name":"Melbourne","flyers_per_mission":540,"cost_per_mission":105},
    {"name":"Brisbane","flyers_per_mission":450,"cost_per_mission":105},
    {"name":"Perth","flyers_per_mission":450,"cost_per_mission":105},
    {"name":"Adelaide","flyers_per_mission":450,"cost_per_mission":105},
    {"name":"Auckland","flyers_per_mission":450,"cost_per_mission":115},
    {"name":"Canberra","flyers_per_mission":300,"cost_per_mission":105},
    {"name":"Newcastle","flyers_per_mission":300,"cost_per_mission":105},
    {"name":"Christchurch","flyers_per_mission":300,"cost_per_mission":115},
    {"name":"Wellington","flyers_per_mission":375,"cost_per_mission":115}
  ]
