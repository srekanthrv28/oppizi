//@flow

export const defaultCitiesList = [
  {"name":"Sydney","latitude":-34.928499,"longitude":138.600746, "flyering_cocations": 300, "county": "Australia, NSW", "population": "4.03m", "pic_url": "https://oppizi.com/wp-content/uploads/2017/09/sydney.jpg", selected: false, budget: 0},
  {"name":"Melbourne","latitude":-37.813628,"longitude":144.963058, "flyering_cocations": 200, "county": "Australia, VIC", "population": "3.85m", "pic_url": "https://oppizi.com/wp-content/uploads/2017/09/melbourne.jpg", selected: false, budget: 0},
  {"name":"Brisbane","latitude":-27.469771,"longitude":153.025124, "flyering_cocations": 150, "county": "Australia, QLD", "population": "1.98m", "pic_url" : "https://oppizi.com/wp-content/uploads/2017/09/brisbane.jpeg", selected: false, budget: 0},
  {"name":"Perth","latitude":-31.950527,"longitude":115.860457, "flyering_cocations": 125, "county": "Australia, WA", "population": "1.67m", "pic_url" : "https://oppizi.com/wp-content/uploads/2017/09/brisbane.jpeg", selected: false, budget: 0},
  {"name":"Auckland","latitude":-36.848460,"longitude":174.763332, "flyering_cocations": 100, "county": "New Zealand", "population": " 1.38m", "pic_url" : "https://oppizi.com/wp-content/uploads/2017/09/auckland.jpg", selected: false, budget: 0},
  {"name":"Adelaide","latitude":-34.928499,"longitude":138.600746, "flyering_cocations": 150, "county": "Australia, SA", "population": "1.20m", "pic_url" : "https://oppizi.com/wp-content/uploads/2017/09/adelaide.jpg", selected: false, budget: 0},
  {"name":"Newcastle","latitude":54.978252,"longitude":-1.617780, "flyering_cocations": 75, "county": "Australia, NSW", "population": "547k", "pic_url" : "https://oppizi.com/wp-content/uploads/2017/09/newcastle.jpg", selected: false, budget: 0},
  {"name":"Christchurch","latitude":-43.532054,"longitude":172.636225, "flyering_cocations": 75, "county": "New Zealand", "population": "348K", "pic_url" : "https://oppizi.com/wp-content/uploads/2017/09/christchurch.jpg", selected: false, budget: 0},
  {"name":"Wellington","latitude":-41.286460,"longitude":174.776236, "flyering_cocations": 75, "county": "Australia, QLD", "population": "204K", "pic_url" : "https://oppizi.com/wp-content/uploads/2017/09/wellington.jpg", selected: false, budget: 0}
]
